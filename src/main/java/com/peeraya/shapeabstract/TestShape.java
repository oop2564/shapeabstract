/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeabstract;

/**
 *
 * @author Administrator
 */
public class TestShape {

    public static void main(String[] args) {
        //วงกลม
        Circle c1 = new Circle(1.5);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(5.5);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);

        Shape[] shapesCircle = {c1, c2, c3};
        for (int i = 0; i < shapesCircle.length; i++) {
            System.out.printf(shapesCircle[i].getName() + " area: " + "%.4f", shapesCircle[i].calArea());
            System.out.println();
        }
        c1.BreakRange();

        //สี่เหลี่ยมผืนผ้า
        Rectangle r1 = new Rectangle(3, 2);
        Rectangle r2 = new Rectangle(4, 3);
        System.out.println(r1);
        System.out.println(r2);

        Shape[] shapesRect = {r1, r2};
        for (int i = 0; i < shapesRect.length; i++) {
            System.out.println(shapesRect[i].getName() + " area: " + shapesRect[i].calArea());
        }
        r1.BreakRange();

        //สี่เหลี่ยมจัตุรัส
        Square s1 = new Square(2);
        Square s2 = new Square(4);
        System.out.println(s1);
        System.out.println(s2);

        Shape[] shapesSquare = {s1, s2};
        for (int i = 0; i < shapesSquare.length; i++) {
            System.out.println(shapesSquare[i].getName() + " area: " + shapesSquare[i].calArea());
        }
        s1.BreakRange();
    }
}
